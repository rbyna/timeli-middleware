package com.intrans.timeli.middleware.mongo.repository;

import com.intrans.timeli.middleware.entities.CameraInventory;
import org.springframework.data.domain.Sort;
import org.springframework.data.geo.Distance;
import org.springframework.data.mongodb.core.geo.GeoJson;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CameraInventoryRepository extends MongoRepository<CameraInventory, String> {

	CameraInventory findByDeviceName(String deviceName);

	List<CameraInventory> findAllByVideoUrlIsNotNull();

	List<CameraInventory> findByVideoUrlIsNotNullAndGeometryNear(GeoJsonPoint point, Distance distance);

	List<CameraInventory> findByVideoUrlIsNotNullAndRouteAndGeometryNear(String route, GeoJsonPoint point, Distance distance);
}
