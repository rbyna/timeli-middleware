package com.intrans.timeli.middleware.mongo.repository;

import com.intrans.timeli.middleware.entities.SegmentLocationData;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface SegmentLocationDataRepository extends MongoRepository<SegmentLocationData, String> {

    List<SegmentLocationData> findByCode(String code);

//    List<SegmentLocationData> findByCodeAndStart_timestampAfterAndStart_timestampBefore(String code, String after, String before);


}
