package com.intrans.timeli.middleware.mongo.repository;

import com.intrans.timeli.middleware.entities.Workzone;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface WorkzoneRepository extends MongoRepository<Workzone, String> {

}
