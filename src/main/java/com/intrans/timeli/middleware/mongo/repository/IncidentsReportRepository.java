package com.intrans.timeli.middleware.mongo.repository;

import com.intrans.timeli.middleware.entities.IncidentsReport;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface IncidentsReportRepository extends MongoRepository<IncidentsReport, String>{


}
