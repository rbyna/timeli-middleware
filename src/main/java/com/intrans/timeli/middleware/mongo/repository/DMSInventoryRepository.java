package com.intrans.timeli.middleware.mongo.repository;


import com.intrans.timeli.middleware.entities.DMSInventory;
import org.springframework.data.geo.Distance;
import org.springframework.data.mongodb.core.geo.GeoJson;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Mongo DAO for accessing the {@link com.intrans.timeli.middleware.entities.DMSInventory} collection.
 * 
 * @author krishnaj
 *
 */
public interface DMSInventoryRepository extends MongoRepository<DMSInventory, String> {

	List<DMSInventory> findByGeometryNear(GeoJsonPoint point, Distance distance);

	List<DMSInventory> findByRouteDesignatorAndGeometryNear(String routeDesignator, GeoJsonPoint point, Distance distance);

}
