package com.intrans.timeli.middleware.mongo.repository;

import com.intrans.timeli.middleware.entities.InrixData;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface InrixDataDao extends MongoRepository<InrixData, String> {

//    InrixData findByCode(String code);
    List<InrixData> findByCode(String code);
}
