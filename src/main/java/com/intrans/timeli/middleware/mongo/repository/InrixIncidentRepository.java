package com.intrans.timeli.middleware.mongo.repository;

import com.intrans.timeli.middleware.entities.InrixIncidents;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface InrixIncidentRepository extends MongoRepository<InrixIncidents, String> {

    InrixIncidents findByCode(String code);

    InrixIncidents deleteInrixIncidentsByCode(String code);

    List<InrixIncidents> findByCurrentIncidentEquals(int currentIncident);

}
