package com.intrans.timeli.middleware.controllers;


import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.intrans.timeli.middleware.entities.CameraInventory;
import com.intrans.timeli.middleware.entities.DMSInventory;
import com.intrans.timeli.middleware.entities.InrixIncidents;
import com.intrans.timeli.middleware.exception.DMSInventoryException;
import com.intrans.timeli.middleware.mongo.repository.DMSInventoryRepository;
import com.intrans.timeli.middleware.mongo.repository.InrixIncidentRepository;
import com.intrans.timeli.middleware.utils.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.geo.GeoJsonLineString;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@RequestMapping(value = { "/api" })
@Controller
public class DMSInventoryController {

    private Cache<String, List<DMSInventory>> dmsDataCache;

    @Autowired
    private InrixIncidentRepository inrixIncidentRepository;
    @Autowired
    private DMSInventoryRepository dmsInventoryRepository;

    @PostConstruct
    public void initializeCache(){
        this.dmsDataCache = CacheBuilder.newBuilder().expireAfterWrite(12, TimeUnit.HOURS)
                .build();
    }

    @Retryable
    @RequestMapping(value = "/getDMSData", method = { RequestMethod.GET})
    public @ResponseBody ResponseEntity<List<DMSInventory>> getDmsData() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Allow-Origin", "*");

        try {
            if(dmsDataCache.size() == 0) {
                List<DMSInventory> dmsInventories = dmsInventoryRepository.findAll();
                dmsDataCache.put("dmsData", dmsInventories);
                ResponseEntity<List<DMSInventory>> responseEntity =
                        new ResponseEntity<List<DMSInventory>>(dmsInventories, headers, HttpStatus.OK);
                return responseEntity;
            } else {
                List<DMSInventory> dmsDataFromCache = getDmsDataFromCache();
                ResponseEntity<List<DMSInventory>> responseEntity =
                        new ResponseEntity<List<DMSInventory>>(dmsDataFromCache, headers, HttpStatus.OK);
                return responseEntity;
            }
        } catch (Exception e) {
            if(ExceptionUtils.handlingException(e)){
                return null;
            }
            throw new DMSInventoryException(e.getCause());
        }
    }

    private List<DMSInventory> getDmsDataFromCache() throws ExecutionException {
        return dmsDataCache.get("dmsData", new Callable<List<DMSInventory>>() {
            @Override
            public List<DMSInventory> call() throws Exception {
                return dmsInventoryRepository.findAll();
            }
        });
    }

    @Retryable
    @RequestMapping(value = "/getNearestDMSData", method = { RequestMethod.GET})
    public @ResponseBody ResponseEntity<List<DMSInventory>> getDmsDataWithin(@RequestParam Double longitude, @RequestParam Double latitude,
                                                                   @RequestParam double distance, @RequestParam String route) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Allow-Origin", "*");

        GeoJsonPoint geoJsonPoint = new GeoJsonPoint(longitude, latitude);
        try {
//            List<DMSInventory> dmsInventories = dmsInventoryRepository.findByGeometryNear(geoJsonPoint,
//                    new Distance(distance));
            String[] roads = route.split(";");
            List<DMSInventory> dmsInventories = new ArrayList<>();
            for (String road : roads) {

                List<DMSInventory> dmsInventoryList = dmsInventoryRepository.findByRouteDesignatorAndGeometryNear(road, geoJsonPoint,
                        new Distance(distance));
                if(dmsInventoryList.size() != 0) {
                    dmsInventories.add(dmsInventoryList.get(0));
                    break;
                }
            }
            if(dmsInventories.size() == 0) {
                List<DMSInventory> byGeometryNear = dmsInventoryRepository.findByGeometryNear(geoJsonPoint,
                        new Distance(distance));
                if(dmsInventories.size() != 0) {
                    dmsInventories.add(byGeometryNear.get(0));
                }
            }
            ResponseEntity<List<DMSInventory>> responseEntity =
                    new ResponseEntity<List<DMSInventory>>(dmsInventories, headers, HttpStatus.OK);
            return responseEntity;
        } catch (Exception e) {
            if(ExceptionUtils.handlingException(e)){
                return null;
            }
            throw new DMSInventoryException(geoJsonPoint, e.getCause());
        }
    }

    @Retryable
    @RequestMapping(value = "/getNearestDMSDataForCode", method = { RequestMethod.GET})
    public @ResponseBody ResponseEntity<List<DMSInventory>> getDmsDataWithin(@RequestParam String code) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Allow-Origin", "*");

        InrixIncidents incidents = inrixIncidentRepository.findByCode(code);
//        GeoJsonLineString geoJsonLineString = new GeoJsonLineString(new Point(incidents.getStart_long(), incidents.getStart_lat()),
//                new Point(incidents.getEnd_long(), incidents.getEnd_lat()));
        GeoJsonPoint startPoint = new GeoJsonPoint(new Point(incidents.getStart_long(), incidents.getStart_lat()));
        GeoJsonPoint endPoint = new GeoJsonPoint(new Point(incidents.getEnd_long(), incidents.getEnd_lat()));

        try {
            List<DMSInventory> dmsInventoriesFromStartPoint = dmsInventoryRepository.findByGeometryNear(startPoint,
                    new Distance(12000));
            List<DMSInventory> dmsInventoriesFromEndPoint = dmsInventoryRepository.findByGeometryNear(endPoint, new Distance(12000));
            dmsInventoriesFromStartPoint.addAll(dmsInventoriesFromEndPoint);
            System.out.println(dmsInventoriesFromStartPoint.size());
            ResponseEntity<List<DMSInventory>> responseEntity =
                    new ResponseEntity<List<DMSInventory>>(dmsInventoriesFromStartPoint, headers, HttpStatus.OK);
            return responseEntity;
        } catch (Exception e) {
            if(ExceptionUtils.handlingException(e)){
                return null;
            }
            throw new DMSInventoryException(startPoint, e.getCause());
        }
    }

    }
