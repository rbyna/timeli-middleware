package com.intrans.timeli.middleware.controllers;

import com.intrans.timeli.middleware.entities.InrixData;
import com.intrans.timeli.middleware.entities.SegmentLocationData;
import com.intrans.timeli.middleware.mongo.repository.InrixDataDao;
import com.intrans.timeli.middleware.mongo.repository.SegmentLocationDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.swing.text.Segment;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RequestMapping(value = { "/api" })
@Controller
public class InrixDataController {

    @Autowired
    private InrixDataDao inrixDataDao;

    @Autowired
    private SegmentLocationDataRepository segmentLocationDataRepository;

    @RequestMapping(value = "/getInrixDataa", method = { RequestMethod.GET})
    public @ResponseBody
    ResponseEntity<Collection<InrixData>> getIncidents(@RequestParam String code) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Allow-Origin", "*");
        try {
            List<InrixData> inrixdata = inrixDataDao.findByCode(code);
//            System.out.println(inrixdata);
            ResponseEntity<Collection<InrixData>> responseEntity =
                    new ResponseEntity<Collection<InrixData>>(inrixdata, headers, HttpStatus.OK);
//            ResponseEntity<List<InrixIncidents>> responseEntity =
//                    new ResponseEntity<List<InrixIncidents>>(incidents, headers, HttpStatus.OK);

            return responseEntity;
        } catch (Exception e) {
            System.out.println("Failed to get incidents");
        }
        return null;
    }

    @RequestMapping(value = "/getInrixData", method = { RequestMethod.GET})
    public @ResponseBody
    ResponseEntity<Collection<SegmentLocationData>> getIncidentsByInterval(@RequestParam String code, @RequestParam String startTime,@RequestParam String endTime) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Allow-Origin", "*");
        List<SegmentLocationData> inrixdata1 = new ArrayList<>();
        try {
            List<SegmentLocationData> inrixdata = segmentLocationDataRepository.findByCode(code);
//            List<SegmentLocationData> inrixdata = segmentLocationDataRepository.findByCodeAndStart_timestampAfterAndStart_timestampBefore(code, "2018-04-29 00:53:10 UTC", "2018-04-29 00:54:10 UTC");
            for(SegmentLocationData segmentLocationData : inrixdata) {
                if(segmentLocationData.getStart_timestamp().compareTo(startTime) > -1 && segmentLocationData.getStart_timestamp().compareTo(endTime) < 1 ) {
                  inrixdata1.add(segmentLocationData);
                }
            }
//  System.out.println(inrixdata);
            ResponseEntity<Collection<SegmentLocationData>> responseEntity =
                    new ResponseEntity<Collection<SegmentLocationData>>(inrixdata1, headers, HttpStatus.OK);
//            ResponseEntity<List<InrixIncidents>> responseEntity =
//                    new ResponseEntity<List<InrixIncidents>>(incidents, headers, HttpStatus.OK);

            return responseEntity;
        } catch (Exception e) {
            System.out.println("Failed to get incidents");
        }
        return null;
    }

    @RequestMapping(value = "/getInrixDatatop", method = { RequestMethod.GET})
    public @ResponseBody
    ResponseEntity<Collection<SegmentLocationData>> getIncidentsTop(@RequestParam String code) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Allow-Origin", "*");
        List<SegmentLocationData> inrixdata1 = new ArrayList<>();
        try {
            List<SegmentLocationData> inrixdata = segmentLocationDataRepository.findByCode(code);
//            List<SegmentLocationData> inrixdata = segmentLocationDataRepository.findByCodeAndStart_timestampAfterAndStart_timestampBefore(code, "2018-04-29 00:53:10 UTC", "2018-04-29 00:54:10 UTC");
            int count = 1;

            for(SegmentLocationData segmentLocationData : inrixdata) {
                if(count < 61) {
//                if(segmentLocationData.getStart_timestamp().compareTo(startTime) > -1 && segmentLocationData.getStart_timestamp().compareTo(endTime) < 1 ) {
                    inrixdata1.add(segmentLocationData);
//                }
                    count++;
                } else {
                    break;
                }
            }
            System.out.println(inrixdata1.size());
//  System.out.println(inrixdata);
            ResponseEntity<Collection<SegmentLocationData>> responseEntity =
                    new ResponseEntity<Collection<SegmentLocationData>>(inrixdata1, headers, HttpStatus.OK);
//            ResponseEntity<List<InrixIncidents>> responseEntity =
//                    new ResponseEntity<List<InrixIncidents>>(incidents, headers, HttpStatus.OK);

            return responseEntity;
        } catch (Exception e) {
            System.out.println("Failed to get incidents");
        }
        return null;
    }


    private static LocalDateTime getDateTime(String startTimestamp) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss z");
        LocalDateTime datetime = null;
        try {
            datetime = LocalDateTime.parse(startTimestamp, formatter);
        } catch (Exception e) {
            System.out.println("cannot parse date");
        }
        return datetime;
    }

    public static void main(String[] args) {
        String date = "2018-04-29 00:53:10 UTC";
        String date2 = "2018-04-29 00:54:10 UTC";
        int i = date2.compareTo(date);
        LocalDateTime dateTime = getDateTime(date);
//        int i = dateTime.compareTo(getDateTime("2018-04-29 00:53:11 UTC"));
        LocalDateTime localDateTime = dateTime.plusHours(1);
        System.out.println(i);
    }

    @RequestMapping(value = "/getInrixDatac", method = { RequestMethod.GET})
    public @ResponseBody
    ResponseEntity<Collection<SegmentLocationData>> getIncidentsCount(@RequestParam String code) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Allow-Origin", "*");
        List<SegmentLocationData> inrixdata1 = new ArrayList<>();
        try {
            List<SegmentLocationData> inrixdata = segmentLocationDataRepository.findByCode(code);
//            List<SegmentLocationData> inrixdata = segmentLocationDataRepository.findByCodeAndStart_timestampAfterAndStart_timestampBefore(code, "2018-04-29 00:53:10 UTC", "2018-04-29 00:54:10 UTC");
            int count = 1;

            for(SegmentLocationData segmentLocationData : inrixdata) {
                if(count < 61) {
//                if(segmentLocationData.getStart_timestamp().compareTo(startTime) > -1 && segmentLocationData.getStart_timestamp().compareTo(endTime) < 1 ) {
                    inrixdata1.add(segmentLocationData);
//                }
                    count++;
                } else {
                    break;
                }
            }
            System.out.println(inrixdata1.size());
//  System.out.println(inrixdata);
            ResponseEntity<Collection<SegmentLocationData>> responseEntity =
                    new ResponseEntity<Collection<SegmentLocationData>>(inrixdata1, headers, HttpStatus.OK);
//            ResponseEntity<List<InrixIncidents>> responseEntity =
//                    new ResponseEntity<List<InrixIncidents>>(incidents, headers, HttpStatus.OK);

            return responseEntity;
        } catch (Exception e) {
            System.out.println("Failed to get incidents");
        }
        return null;
    }



}
