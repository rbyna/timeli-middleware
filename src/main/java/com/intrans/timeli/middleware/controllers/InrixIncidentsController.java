package com.intrans.timeli.middleware.controllers;


import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.intrans.timeli.middleware.entities.DMSInventory;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import com.intrans.timeli.middleware.entities.InrixIncidents;
import com.intrans.timeli.middleware.exception.InrixIncidentException;
import com.intrans.timeli.middleware.mongo.repository.InrixIncidentRepository;
import com.intrans.timeli.middleware.utils.ExceptionUtils;
import com.mongodb.DBCollection;
import com.mongodb.MongoCursorNotFoundException;
import com.mongodb.client.AggregateIterable;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.Local;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@RequestMapping(value = { "/api" })
@Controller
public class InrixIncidentsController {

    @Autowired
    private InrixIncidentRepository inrixIncidentRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Retryable
    @RequestMapping(value = "/getIncidents", method = { RequestMethod.GET})
    public @ResponseBody ResponseEntity<Collection<InrixIncidents>> getIncidents() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Allow-Origin", "*");
        int count = 0;
        try {
            List<InrixIncidents> incidents = inrixIncidentRepository.findByCurrentIncidentEquals(1);
            System.out.println(incidents.size());
            Map<String, InrixIncidents> incidentsMap = new HashMap<>();


            for (InrixIncidents incident : incidents) {
                incidentsMap.put(incident.getEvent_id(), incident);
            }

            Collection<InrixIncidents> values = incidentsMap.values();
            ResponseEntity<Collection<InrixIncidents>> responseEntity =
                    new ResponseEntity<Collection<InrixIncidents>>(values, headers, HttpStatus.OK);
//            ResponseEntity<List<InrixIncidents>> responseEntity =
//                    new ResponseEntity<List<InrixIncidents>>(incidents, headers, HttpStatus.OK);
            System.out.println(values.size());
            return responseEntity;
        } catch (Exception e) {
            if(ExceptionUtils.handlingException(e)){
                return null;
            }
            throw new InrixIncidentException(e.getCause());
        }
    }

    @Retryable
    @RequestMapping(value = "/getIncidentsDetails", method = { RequestMethod.GET})
    public @ResponseBody ResponseEntity<InrixIncidents> getIncidentDetails(@RequestParam String code) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Allow-Origin", "*");

        try {
            InrixIncidents incidents = inrixIncidentRepository.findByCode(code);
            ResponseEntity<InrixIncidents> responseEntity =
                    new ResponseEntity<InrixIncidents>(incidents, headers, HttpStatus.OK);
            return responseEntity;
        } catch (Exception e) {
            if(ExceptionUtils.handlingException(e)){
                return null;
            }
            throw new InrixIncidentException(code, e.getCause());
        }
    }

    @Retryable
    @RequestMapping(value = "/removeIncident", method = {RequestMethod.POST})
    public @ResponseBody ResponseEntity<HttpStatus> removeIncident(@RequestBody String code) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Allow-Origin", "*");
        try {
            inrixIncidentRepository.deleteInrixIncidentsByCode(code);
        } catch (Exception e) {
            if(ExceptionUtils.handlingException(e)){
                return null;
            }
            throw new InrixIncidentException(code, e.getCause());
        }
        return new ResponseEntity<>(headers, HttpStatus.OK);
    }

    @Retryable
    @RequestMapping(value = "/falseIncident", method = {RequestMethod.POST})
    public @ResponseBody ResponseEntity<InrixIncidents> falseIncident(@RequestBody InrixIncidents incidents) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Allow-Origin", "*");
        InrixIncidents inrixIncidents = inrixIncidentRepository.findByCode(incidents.getCode());
        try {
            inrixIncidents.setCurrentIncident(0);
            inrixIncidents.setFalseIncident(true);
            inrixIncidents.setIncident_inprogess(false);
            //Update end timestamp
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss z");
            ZonedDateTime date = ZonedDateTime.now(ZoneId.of("UTC"));
            String formatDateTime = date.format(formatter);
            inrixIncidents.setEnd_timestamp(formatDateTime);
            inrixIncidentRepository.save(inrixIncidents);
        } catch (Exception e) {
            if(ExceptionUtils.handlingException(e)){
                return null;
            }
            throw new InrixIncidentException(inrixIncidents, e.getCause());
        }
        return new ResponseEntity<>(inrixIncidents, headers, HttpStatus.OK);
    }

    @Retryable
    @RequestMapping(value="/incidentInprogress", method = {RequestMethod.POST})
    public  @ResponseBody ResponseEntity<InrixIncidents> incidentInprogress(@RequestBody String code) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Allow-Origin", "*");
        InrixIncidents incident = inrixIncidentRepository.findByCode(code);
        try {

            incident.setIncident_inprogess(true);
            inrixIncidentRepository.save(incident);
        } catch (Exception e) {
            if(ExceptionUtils.handlingException(e)){
                return null;
            }
            throw new InrixIncidentException(code, e.getCause());
        }
        return new ResponseEntity<>(incident, headers, HttpStatus.OK);
    }
    //Aggregation Testing
    @RequestMapping(value = "/getIncidentss", method = { RequestMethod.GET})
    public @ResponseBody ResponseEntity<List<HostingCount>> getIncidentss() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Allow-Origin", "*");

        Aggregation agg = Aggregation.newAggregation(
                match(Criteria.where("currentIncident").is(1)),
//                group("event_id").count().as("total"),
                group("event_id").push("code").as("codes").push("fid").as("fid"),
                project("codes", "fid").and("event_id").previousOperation()
//                sort(Sort.Direction.DESC, "total")

        );

        //Convert the aggregation result into a List
        AggregationResults<HostingCount> groupResults
                = mongoTemplate.aggregate(agg, "inrixdata", HostingCount.class);

        List<HostingCount> result = groupResults.getMappedResults();

        System.out.println("Events : " + result.size());
        return new ResponseEntity<>(result, headers, HttpStatus.OK);
    }



    public class HostingCount implements Serializable{
        public HostingCount() {

        }

        public String getEvent_id() {
            return event_id;
        }

        public void setEvent_id(String event_id) {
            this.event_id = event_id;
        }

        private String event_id;

        public List<String> getCodes() {
            return codes;
        }

        public void setCodes(List<String> codes) {
            this.codes = codes;
        }

        private List<String> codes;

        public int getFid() {
            return fid;
        }

        public void setFid(int fid) {
            this.fid = fid;
        }

        private int fid;

//        public long getTotal() {
//            return total;
//        }
//
//        public void setTotal(long total) {
//            this.total = total;
//        }

        //...
    }


}
