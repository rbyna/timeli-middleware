package com.intrans.timeli.middleware.controllers;

import com.intrans.timeli.middleware.entities.Workzone;
import com.intrans.timeli.middleware.mongo.repository.WorkzoneRepository;
import com.mongodb.DBCursor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.retry.annotation.Retryable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
public class WorkzoneService {

    @Autowired
    private WorkzoneRepository workzoneRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Retryable
    public @ResponseBody
    ResponseEntity<List<Workzone>> getWorkzoneDetails() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Allow-Origin", "*");
        List<Workzone> workzones = new ArrayList<>();
//        try {
//            List<Workzone> workzones = workzoneRepository.findAll();
//            AnnotationConfigApplicationContext ctx =
//                    new AnnotationConfigApplicationContext();
//            ctx.register(MongoTemplate.class);
//            ctx.refresh();
//            boolean mongoTemplate = ctx.containsBean("mongoTemplate");
//            System.out.println(mongoTemplate);
//            MongoOperations mongoOperation = (MongoOperations) ctx.getBean("mongoTemplate");

//            mongoTemplate.getDb().addOption(QUERYOPTION_NOTIMEOUT);

//            List<Workzone> workzones = mongoTemplate.findAll(Workzone.class);
//            mongoTemplate.getCollection("").find().batchSize(100);
        DBCursor workzone_inventory = mongoTemplate.getCollection("workzone_inventory").find().batchSize(100).maxTime(60, TimeUnit.MINUTES);
        while (workzone_inventory.hasNext()){
            Workzone read = mongoTemplate.getConverter().read(Workzone.class, workzone_inventory.next());
            workzones.add(read);
        }


//            List<Workzone> workzones = mongoOperation.findAll(Workzone.class);
        System.out.println(workzones.size());
        return new ResponseEntity<>(workzones, headers, HttpStatus.OK);
//        } catch (Exception e) {
//            if(e instanceof MongoCursorNotFoundException) {
//                return getWorkzoneDetails();
//            } else {
//                throw new WorkzoneInventoryException(e.getCause());
//            }
//        }
    }

}
