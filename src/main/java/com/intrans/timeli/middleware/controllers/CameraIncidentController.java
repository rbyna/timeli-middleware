package com.intrans.timeli.middleware.controllers;

import com.intrans.timeli.middleware.entities.IncidentDetection;
import com.intrans.timeli.middleware.queries.Queries;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@RequestMapping(value = { "/api" })
@Controller
public class CameraIncidentController {

    @RequestMapping(value="/test", method=RequestMethod.GET)
    @ResponseBody
    public String testService()
    {
        return "The service is running.";

    }

    @RequestMapping(value="/cameraincidents", method=RequestMethod.GET)
    @ResponseBody
    public List<IncidentDetection> getIncidents(@RequestParam("start") String startDate, @RequestParam(value="end",required=false) String endDate)
    {
        System.out.println(startDate);

        Queries q = new Queries();
        if(endDate != null)
            return q.getIncidentsBetweenTwoDates(startDate, endDate);
        else
            return q.getIncidentsAfterDate(startDate);
    }
}
