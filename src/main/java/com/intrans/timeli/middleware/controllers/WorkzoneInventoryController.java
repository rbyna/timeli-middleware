package com.intrans.timeli.middleware.controllers;

import com.intrans.timeli.middleware.entities.Workzone;
import com.intrans.timeli.middleware.exception.WorkzoneInventoryException;
import com.intrans.timeli.middleware.mongo.repository.WorkzoneRepository;
import com.intrans.timeli.middleware.utils.ExceptionUtils;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoCursorNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.NestedServletException;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.mongodb.Bytes.QUERYOPTION_NOTIMEOUT;

@RequestMapping(value = { "/api" })
@Controller
public class WorkzoneInventoryController {

    @Autowired
    private WorkzoneRepository workzoneRepository;
//
//    @Autowired
//    private MongoTemplate mongoTemplate;
//
//    @Autowired
//    private WorkzoneService workzoneService;

    @Retryable
    @RequestMapping(value = "/workzonexd", method = { RequestMethod.GET})
    public @ResponseBody ResponseEntity<List<Workzone>> getWorkzoneDetails() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Allow-Origin", "*");

        try {
            List<Workzone> workzones = workzoneRepository.findAll();
            return new ResponseEntity<>(workzones, headers, HttpStatus.OK);
        } catch (Exception e) {
            if(ExceptionUtils.handlingException(e)){
                return null;
            }
            throw new WorkzoneInventoryException(e.getCause());
        }
    }

/*//    @Retryable(value = {MongoCursorNotFoundException.class, NestedServletException.class}, maxAttempts = 3)
    @RequestMapping(value = "/workzonexd", method = { RequestMethod.GET})
    @Retryable
    public @ResponseBody ResponseEntity<List<Workzone>> getWorkzoneDetails() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Allow-Origin", "*");
        List<Workzone> workzones = new ArrayList<>();
//        try {
//            List<Workzone> workzones = workzoneRepository.findAll();
//            AnnotationConfigApplicationContext ctx =
//                    new AnnotationConfigApplicationContext();
//            ctx.register(MongoTemplate.class);
//            ctx.refresh();
//            boolean mongoTemplate = ctx.containsBean("mongoTemplate");
//            System.out.println(mongoTemplate);
//            MongoOperations mongoOperation = (MongoOperations) ctx.getBean("mongoTemplate");

//            mongoTemplate.getDb().addOption(QUERYOPTION_NOTIMEOUT);

//            List<Workzone> workzones = mongoTemplate.findAll(Workzone.class);
//            mongoTemplate.getCollection("").find().batchSize(100);
            DBCursor workzone_inventory = mongoTemplate.getCollection("workzone_inventory").find().batchSize(100).maxTime(60, TimeUnit.MINUTES);
            while (workzone_inventory.hasNext()){
                Workzone read = mongoTemplate.getConverter().read(Workzone.class, workzone_inventory.next());
                workzones.add(read);
            }


//            List<Workzone> workzones = mongoOperation.findAll(Workzone.class);
            System.out.println(workzones.size());
            return new ResponseEntity<>(workzones, headers, HttpStatus.OK);
//        } catch (Exception e) {
//            if(e instanceof MongoCursorNotFoundException) {
//                return getWorkzoneDetails();
//            } else {
//                throw new WorkzoneInventoryException(e.getCause());
//            }
//        }
    }*/

//    @RequestMapping(value = "/workzonexd", method = { RequestMethod.GET})
//    @Retryable
//    public @ResponseBody ResponseEntity<List<Workzone>> getWorkzoneDetails() {
//        ResponseEntity<List<Workzone>> workzoneDetails = workzoneService.getWorkzoneDetails();
//        return workzoneDetails;
//    }
}
