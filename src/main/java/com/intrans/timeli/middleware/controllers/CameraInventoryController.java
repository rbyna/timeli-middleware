package com.intrans.timeli.middleware.controllers;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.intrans.timeli.middleware.entities.CameraInventory;
import com.intrans.timeli.middleware.entities.InrixIncidents;
import com.intrans.timeli.middleware.entities.VideoUrl;
import com.intrans.timeli.middleware.exception.CameraInventoryException;
import com.intrans.timeli.middleware.mongo.repository.CameraInventoryRepository;
import com.intrans.timeli.middleware.mongo.repository.InrixIncidentRepository;
import com.intrans.timeli.middleware.utils.HttpUtils;
import com.mongodb.MongoCursorNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.geo.GeoJsonLineString;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.intrans.timeli.middleware.utils.ExceptionUtils.handlingException;

@RequestMapping(value = { "/api" })
@Controller
public class CameraInventoryController {

    private Cache<String, List<CameraInventory>> cameraDetailsCache;

    @Autowired
    private InrixIncidentRepository inrixIncidentRepository;

    @Autowired
    private CameraInventoryRepository cameraInventoryRepository;

    @PostConstruct
    public void initializeCache(){
        this.cameraDetailsCache = CacheBuilder.newBuilder().expireAfterWrite(12, TimeUnit.HOURS)
                .build();
    }

    @Retryable
    @RequestMapping(value = "/getCameraLocations", method = { RequestMethod.GET})
    public @ResponseBody ResponseEntity<List<CameraInventory>> getCameraDetailsFromCache() {
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpUtils.addHeaders(httpHeaders, "Access-Control-Allow-Origin", "*");
        try {
            if(cameraDetailsCache.size() == 0) {
                List<CameraInventory> cameraDetails = cameraInventoryRepository.findAllByVideoUrlIsNotNull();
                cameraDetailsCache.put("cameradetails", cameraDetails);

                ResponseEntity<List<CameraInventory>> responseEntity =
                        new ResponseEntity<List<CameraInventory>>(cameraDetails, httpHeaders, HttpStatus.OK);
                return responseEntity;
            } else {
                List<CameraInventory> cameraInventoryDetailsFromCache = getCameraInventoryDetailsFromCache();
                ResponseEntity<List<CameraInventory>> responseEntity =
                        new ResponseEntity<List<CameraInventory>>(cameraInventoryDetailsFromCache, httpHeaders, HttpStatus.OK);
                return responseEntity;
            }
        } catch (Exception e) {
            if(handlingException(e)){
                return null;
            } else {
                throw new CameraInventoryException(e.getCause().getMessage(), e.getCause());
            }
        }
    }

    private List<CameraInventory> getCameraInventoryDetailsFromCache() throws ExecutionException {
        return cameraDetailsCache.get("cameradetails", new Callable<List<CameraInventory>>() {
            @Override
            public List<CameraInventory> call() throws Exception {
                return cameraInventoryRepository.findAllByVideoUrlIsNotNull();
            }
        });

    }

    @Retryable
    @RequestMapping(value = "/getnearestCameras", method = { RequestMethod.GET})
    public @ResponseBody ResponseEntity<List<CameraInventory>> getCameraDetailsWithin(@RequestParam Double longitude, @RequestParam Double latitude,
                                                                      @RequestParam double distance, @RequestParam String route) {
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpUtils.addHeaders(httpHeaders, "Access-Control-Allow-Origin", "*");
        final GeoJsonPoint geoJsonPoint = new GeoJsonPoint(longitude, latitude);
        try {
//            List<CameraInventory> cameraDetails = cameraInventoryRepository.findByGeometryNear(geoJsonPoint,
//                    new Distance(distance));
            String[] roads = route.split(";");
            List<CameraInventory> cameraDetails = new ArrayList<>();
            for (String road : roads) {

                //road names can have more components other than letter and number
                //we are interested only in letter and number that are first two components
                String[] splits = road.split(" ");

                String roadName = splits[0] + "-" + splits[1];

                List<CameraInventory> cameraInventories = cameraInventoryRepository
                        .findByVideoUrlIsNotNullAndRouteAndGeometryNear(roadName, geoJsonPoint, new Distance(distance));
                if(cameraInventories.size() != 0) {
                    cameraDetails.add(cameraInventories.get(0));
                    break;
                }
            }
            if(cameraDetails.size() == 0) {
                List<CameraInventory> byGeometryNear = cameraInventoryRepository.findByVideoUrlIsNotNullAndGeometryNear(geoJsonPoint,
                    new Distance(distance));
                if(cameraDetails.size() != 0) {
                    cameraDetails.add(byGeometryNear.get(0));
                }
            }
            ResponseEntity<List<CameraInventory>> responseEntity =
                    new ResponseEntity<List<CameraInventory>>(cameraDetails, httpHeaders, HttpStatus.OK);
            return responseEntity;
        } catch (Exception e) {
            if(handlingException(e)){
                return null;
            } else {
                throw new CameraInventoryException(geoJsonPoint, e.getCause());
            }
        }
    }

    @Retryable
    @RequestMapping(value = "/getnearestCamerasforcode", method = { RequestMethod.GET})
    public @ResponseBody ResponseEntity<List<CameraInventory>> getCameraDetailsWithin(@RequestParam String code) {
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpUtils.addHeaders(httpHeaders, "Access-Control-Allow-Origin", "*");
        InrixIncidents incidents = inrixIncidentRepository.findByCode(code);
        GeoJsonPoint startPoint = new GeoJsonPoint(new Point(incidents.getStart_long(), incidents.getStart_lat()));
        GeoJsonPoint endPoint = new GeoJsonPoint(new Point(incidents.getEnd_long(), incidents.getEnd_lat()));
        try {

            List<CameraInventory> cameraDetailsFromStartPoint = cameraInventoryRepository.findByVideoUrlIsNotNullAndGeometryNear(startPoint, new Distance(12000));
            List<CameraInventory> cameraDetailsFromEndPoint = cameraInventoryRepository.findByVideoUrlIsNotNullAndGeometryNear(endPoint, new Distance(12000));
            cameraDetailsFromStartPoint.addAll(cameraDetailsFromEndPoint);
            System.out.println(cameraDetailsFromStartPoint.size());
            ResponseEntity<List<CameraInventory>> responseEntity =
                    new ResponseEntity<List<CameraInventory>>(cameraDetailsFromStartPoint, httpHeaders, HttpStatus.OK);
            return responseEntity;
        } catch (Exception e) {
            if(handlingException(e)){
                return null;
            } else {
                throw new CameraInventoryException(startPoint, e.getCause());
            }
        }
    }

    @Retryable
    @RequestMapping(value = "/getVideoUrls", method = { RequestMethod.GET})
    public @ResponseBody ResponseEntity<List<VideoUrl>> getVideoUrls() {
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpUtils.addHeaders(httpHeaders, "Access-Control-Allow-Origin", "*");

        try {
            List<CameraInventory> all = getCameraInventoryDetailsFromCache();
            System.out.println(all.size());
            List<VideoUrl> videoUrls = new ArrayList<>();
            all.forEach(new Consumer<CameraInventory>() {
                @Override
                public void accept(CameraInventory cameraInventory) {
                    if(cameraInventory.getVideoUrl() != null) {
                        videoUrls.add(new VideoUrl(cameraInventory.getDeviceName(), cameraInventory.getVideoUrl()));
                    }
                }
            });
            System.out.println(videoUrls.size());
            ResponseEntity<List<VideoUrl>> responseEntity =
                    new ResponseEntity<List<VideoUrl>>(videoUrls, httpHeaders, HttpStatus.OK);
            return responseEntity;
        } catch (Exception e) {
            if(handlingException(e)){
                return null;
            } else {
                throw new CameraInventoryException("Video Urls", e.getCause());
            }
        }
    }

    @Retryable
    @RequestMapping(value = "/getVideoUrlsForCamera", method = { RequestMethod.GET})
    public @ResponseBody ResponseEntity<VideoUrl> getVideoUrlsForCamera(@RequestParam String camera_name) {
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpUtils.addHeaders(httpHeaders, "Access-Control-Allow-Origin", "*");

        try {
            CameraInventory byDeviceName = cameraInventoryRepository.findByDeviceName(camera_name);

            VideoUrl videoUrl = new VideoUrl(byDeviceName.getDeviceName(), byDeviceName.getVideoUrl());
            ResponseEntity<VideoUrl> responseEntity =
                    new ResponseEntity<VideoUrl>(videoUrl, httpHeaders, HttpStatus.OK);
            return responseEntity;
        } catch (Exception e) {
            if(handlingException(e)){
                return null;
            } else {
                throw new CameraInventoryException("Video Urls", camera_name, e.getCause());
            }
        }
    }

//    @RequestMapping(value = "/getCameraLocationsForTest", method = { RequestMethod.GET})
//    public @ResponseBody ResponseEntity<List<CameraInventory>> getCameraDetailsTest() {
//        try {
////            if(cameraDetailsCache.size() == 0) {
//            List<CameraInventory> cameraDetails = cameraInventoryRepository.findAll();
//            cameraDetailsCache.put("cameradetails", cameraDetails);
//            HttpHeaders headers = new HttpHeaders();
//            headers.add("Access-Control-Allow-Origin", "*");
//            ResponseEntity<List<CameraInventory>> responseEntity =
//                    new ResponseEntity<List<CameraInventory>>(cameraDetails, headers, HttpStatus.OK);
//            return responseEntity;
////            } else {
////                return getCameraInventoryDetailsFromCache();
////            }
//        } catch (Exception e) {
//            throw new CameraInventoryException(e.getCause().getMessage(), e.getCause());
//        }
//    }



}
