//package com.intrans.timeli.middleware.schedular;
//
//import com.mongodb.Block;
//import com.mongodb.client.MongoCollection;
//import com.mongodb.client.model.Aggregates;
//import com.mongodb.client.model.Filters;
//import com.mongodb.client.model.changestream.ChangeStreamDocument;
//import com.mongodb.client.model.changestream.FullDocument;
//import org.bson.Document;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.PostConstruct;
//import java.util.Arrays;
//
//@Component
//public class MongoDbWatchJob {
//    @Autowired
//    MongoCollection<Document> personCollection;
//
//    @PostConstruct
//    public void runOnStartup() {
//        executePersonCollectionWatchJob();
//    }
//
//    private void executePersonCollectionWatchJob() {
//        Block<ChangeStreamDocument<Document>> printBlock = new Block<ChangeStreamDocument<Document>>() {
//            @Override
//            public void apply(final ChangeStreamDocument<Document> changeStreamDocument) {
//                System.out.println(changeStreamDocument);
//            }
//        };
//        personCollection.watch().forEach(printBlock);
//        personCollection.watch(Arrays.asList(Aggregates.match(Filters.in("operationType", Arrays.asList("insert", "update", "replace", "delete")))))
//                .fullDocument(FullDocument.UPDATE_LOOKUP).forEach(printBlock);
//    }
//}
