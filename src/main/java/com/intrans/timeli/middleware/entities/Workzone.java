package com.intrans.timeli.middleware.entities;

import com.querydsl.core.annotations.QueryEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@QueryEntity
@Document(collection="workzone_inventory")
public class Workzone {

    //{"objectid":47029,"code":"1485857029","previous_code":"1485857043","next_code":"1485857013","frc":0,"roadnumber":141,
    // "roadname":"IA 141","linearid":0,"country":"United States of America","state":"Iowa","county":"Polk",
    // "district":" ","miles":0.497619,"sliproad":"0","specialroad":" ","roadlist":"IA 141","start_lat":41.7102,
    // "start_long":-93.7745,"end_lat":41.7174,"end_long":-93.7745,"bearing":"N","xdgroup":"3104391","shapesrid":"4326",
    // "workzone2":"1ab","direction2":1,"n_order":9,"segmentcount":9,"direction":"Northbound"}

    //objectid,code,previous_code,next_code,frc,roadnumber,roadname,linearid,country,state,county,district,miles,sliproad,specialroad,roadlist,
    // start_lat,start_long,end_lat,end_long,bearing,xdgroup,shapesrid,workzone2,direction2,n_order,segmentcount,direction


    private String objectid;
    private String code;
    private String previous_code;
    private String next_code;
    private String frc;
    private String roadnumber;
    private String roadname;
    private String linearid;
    private String country;
    private String state;
    private String county;
    private String district;
    private String miles;
    private String sliproad;
    private String specialroad;
    private String roadlist;
    private String start_lat;
    private String start_long;
    private String end_lat;
    private String end_long;
    private String bearing;
    private String xdgroup;
    private String shapesrid;
    private String workzone2;
    private String direction2;
    private String n_order;
    private String segmentcount;
    private String direction;

}
