package com.intrans.timeli.middleware.entities;

public class VideoUrl {

    public VideoUrl(String deviceName, String videoUrl) {
        this.deviceName = deviceName;
        this.videoUrl = videoUrl;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    private String deviceName;

    private String videoUrl;
}
