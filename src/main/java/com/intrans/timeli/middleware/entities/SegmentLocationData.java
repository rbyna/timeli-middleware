package com.intrans.timeli.middleware.entities;

import com.querydsl.core.annotations.QueryEntity;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@QueryEntity
@Document(collection="segmentdata")
public class SegmentLocationData {

    //{"event_id":1485427321,"code":"1485427321","start_timestamp":1520013779000,"end_timestamp":1520013779000,"threshold_speed":45,"speed":17,"avg_speed":17,"author":null,"incident_inprogess":0,"incident_count":15,"currentIncident":1,"fid":3919,"oid_1":10047820,"previous_code":"1485745916","next_code":"1485831410","frc":1,"roadnumber":35,"roadname":"I 35 Business;US 69","linearid":0,"country":"United States of America","state":"Iowa","county":"Story","district":"Ames","miles":0.115159,"sliproad":"0","specialroad":" ","roadlist":"I 35 Business;US 69","start_lat":42.0084,"start_long":-93.6102,"end_lat":42.0067,"end_long":-93.6102,"bearing":"S","xdgroup":"3083344","shapesrid":"4326\r"}
    @Id
    private String _id;
    private String event_id;
    private String code;
    private String start_timestamp;
    private String end_timestamp;
    private String threshold_speed;
    private double speed;
    private double avg_speed;
    private String author;
    private boolean incident_inprogess;
    private int incident_count;
    private int currentIncident;
    private int fid;
    private int oid_1;
    private String previous_code;
    private String next_code;
    private int frc;
    private String roadnumber;
    private String roadname;

    private String linearid;

    private String country;
    private String state;
    private String county;

    private String district;

    private double miles;

    private String slipRoad;
    private String specialroad;
    private String roadlist;

    private double start_lat;
    private double start_long;
    private double end_lat;
    private double end_long;
    private String bearing;
    private String xdgroup;
    private String shapesrid;

    public boolean isFalseIncident() {
        return falseIncident;
    }

    public void setFalseIncident(boolean falseIncident) {
        this.falseIncident = falseIncident;
    }

    private boolean falseIncident;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStart_timestamp() {
        return start_timestamp;
    }

    public void setStart_timestamp(String start_timestamp) {
        this.start_timestamp = start_timestamp;
    }

    public String getEnd_timestamp() {
        return end_timestamp;
    }

    public void setEnd_timestamp(String end_timestamp) {
        this.end_timestamp = end_timestamp;
    }

    public String getThreshold_speed() {
        return threshold_speed;
    }

    public void setThreshold_speed(String threshold_speed) {
        this.threshold_speed = threshold_speed;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getAvg_speed() {
        return avg_speed;
    }

    public void setAvg_speed(double avg_speed) {
        this.avg_speed = avg_speed;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public boolean isIncident_inprogess() {
        return incident_inprogess;
    }

    public void setIncident_inprogess(boolean incident_inprogess) {
        this.incident_inprogess = incident_inprogess;
    }

    public int getIncident_count() {
        return incident_count;
    }

    public void setIncident_count(int incident_count) {
        this.incident_count = incident_count;
    }

    public int getCurrentIncident() {
        return currentIncident;
    }

    public void setCurrentIncident(int currentIncident) {
        this.currentIncident = currentIncident;
    }

    public int getFid() {
        return fid;
    }

    public void setFid(int fid) {
        this.fid = fid;
    }

    public int getOid_1() {
        return oid_1;
    }

    public void setOid_1(int oid_1) {
        this.oid_1 = oid_1;
    }

    public String getPrevious_code() {
        return previous_code;
    }

    public void setPrevious_code(String previous_code) {
        this.previous_code = previous_code;
    }

    public String getNext_code() {
        return next_code;
    }

    public void setNext_code(String next_code) {
        this.next_code = next_code;
    }

    public int getFrc() {
        return frc;
    }

    public void setFrc(int frc) {
        this.frc = frc;
    }

    public String getRoadnumber() {
        return roadnumber;
    }

    public void setRoadnumber(String roadnumber) {
        this.roadnumber = roadnumber;
    }

    public String getRoadname() {
        return roadname;
    }

    public void setRoadname(String roadname) {
        this.roadname = roadname;
    }

    public String getLinearid() {
        return linearid;
    }

    public void setLinearid(String linearid) {
        this.linearid = linearid;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public double getMiles() {
        return miles;
    }

    public void setMiles(double miles) {
        this.miles = miles;
    }

    public String getSlipRoad() {
        return slipRoad;
    }

    public void setSlipRoad(String slipRoad) {
        this.slipRoad = slipRoad;
    }

    public String getSpecialroad() {
        return specialroad;
    }

    public void setSpecialroad(String specialroad) {
        this.specialroad = specialroad;
    }

    public String getRoadlist() {
        return roadlist;
    }

    public void setRoadlist(String roadlist) {
        this.roadlist = roadlist;
    }

    public double getStart_lat() {
        return start_lat;
    }

    public void setStart_lat(double start_lat) {
        this.start_lat = start_lat;
    }

    public double getStart_long() {
        return start_long;
    }

    public void setStart_long(double start_long) {
        this.start_long = start_long;
    }

    public double getEnd_lat() {
        return end_lat;
    }

    public void setEnd_lat(double end_lat) {
        this.end_lat = end_lat;
    }

    public double getEnd_long() {
        return end_long;
    }

    public void setEnd_long(double end_long) {
        this.end_long = end_long;
    }

    public String getBearing() {
        return bearing;
    }

    public void setBearing(String bearing) {
        this.bearing = bearing;
    }

    public String getXdgroup() {
        return xdgroup;
    }

    public void setXdgroup(String xdgroup) {
        this.xdgroup = xdgroup;
    }

    public String getShapesrid() {
        return shapesrid;
    }

    public void setShapesrid(String shapesrid) {
        this.shapesrid = shapesrid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SegmentLocationData incidents = (SegmentLocationData) o;
        return Objects.equals(event_id, incidents.event_id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(event_id);
    }
}
