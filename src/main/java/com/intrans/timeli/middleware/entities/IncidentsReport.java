package com.intrans.timeli.middleware.entities;

import com.querydsl.core.annotations.QueryEntity;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@NoArgsConstructor

@QueryEntity
@Document(collection="incidentreports")
public class IncidentsReport {
}
