package com.intrans.timeli.middleware.utils;

import com.mongodb.MongoCursorNotFoundException;

public class ExceptionUtils {

    public static boolean handlingException(Exception e) {
        if(e instanceof MongoCursorNotFoundException){
            System.out.println("Cursor not found exception. Retrying");
            return true;
        }
        return false;
    }
}
