package com.intrans.timeli.middleware.utils;

import org.springframework.http.HttpHeaders;

public class HttpUtils {

    public static void addHeaders(HttpHeaders httpHeaders, String headerName, String headerValue) {
        httpHeaders.add(headerName, headerValue);
    }

}
