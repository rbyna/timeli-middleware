/**
 * 
 */
package com.intrans.timeli.middleware.utils;

/**
 * @author revanth
 *
 */
public class Constants {
	
	public static final String DB_Driver = "com.mysql.cj.jdbc.Driver";
//	public static final String host = "localhost";
	public static final String host = "13.89.238.76";
	public static final int port = 3306;
	public static final String DB = "videos";
	public static final String DB_Connection = "jdbc:mysql://" + host + ":" + String.valueOf(port) + "/" + DB;
	public static final String DB_User = "root";
	public static final String DB_Password = "password";

}
