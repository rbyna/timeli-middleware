package com.intrans.timeli.middleware.exception;

import org.springframework.data.mongodb.core.geo.GeoJson;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;

public class DMSInventoryException extends RuntimeException {

    public DMSInventoryException(Throwable message) {
        super("Could not retrieve dms details. Error msg : " + message);
    }

    public DMSInventoryException(GeoJson point, Throwable message) {
        super("Could not retrieve nearest dms details from point : " + point + "Error msg : " + message);
    }

}
