package com.intrans.timeli.middleware.exception;

import com.intrans.timeli.middleware.entities.InrixIncidents;

public class InrixIncidentException extends RuntimeException {

    public InrixIncidentException(Throwable message) {
        super("Could not retrieve incidents. Error msg : " + message);
    }

    public InrixIncidentException(String code, Throwable message) {
        super("Could not retrieve incidents for code : " + code + "Error msg : " + message);
    }

    public InrixIncidentException(InrixIncidents inrixIncidents, Throwable message) {
        super("Could not save/update incident : " + inrixIncidents + "Error msg : " + message);
    }
}
