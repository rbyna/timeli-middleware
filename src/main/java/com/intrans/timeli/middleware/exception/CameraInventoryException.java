package com.intrans.timeli.middleware.exception;

import org.springframework.data.mongodb.core.geo.GeoJson;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;

public class CameraInventoryException extends RuntimeException {

    public CameraInventoryException(Throwable cause) {
        super("Could not retrieve camera details. Error msg : " + cause);
    }

    public CameraInventoryException(GeoJson point, Throwable message) {
        super("Could not retrieve nearest camera details from point : " + point + "Error msg : " + message);
    }

    public CameraInventoryException(String details, Throwable message) {
        super("Could not retrieve : " + details + " Error msg : " + message);
    }

    public CameraInventoryException(String details, String cameraName, Throwable message) {
        super("Could not retrieve : " + details + "for camera : " + cameraName + " Error msg : " + message);
    }
}
