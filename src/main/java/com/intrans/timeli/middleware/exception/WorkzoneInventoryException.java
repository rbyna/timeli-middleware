package com.intrans.timeli.middleware.exception;

public class WorkzoneInventoryException extends RuntimeException {

    public WorkzoneInventoryException(Throwable message) {
        super("Could not retrieve workzone details. Error msg : " + message);
    }


}
