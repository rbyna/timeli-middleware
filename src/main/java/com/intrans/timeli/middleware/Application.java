package com.intrans.timeli.middleware;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientURI;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.context.embedded.jetty.JettyEmbeddedServletContainerFactory;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import static com.mongodb.Bytes.QUERYOPTION_NOTIMEOUT;

@SpringBootApplication(scanBasePackages = "com.intrans.timeli.middleware.*")
@EnableMongoRepositories(basePackages = { "com.intrans.timeli.middleware.*" })
@EnableScheduling
@EnableCaching
@EnableAutoConfiguration
@EnableRetry
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

//    @Bean
//    public JettyEmbeddedServletContainerFactory  jettyEmbeddedServletContainerFactory() {
//        JettyEmbeddedServletContainerFactory jettyContainer =
//                new JettyEmbeddedServletContainerFactory();
//
//        jettyContainer.setPort(3000);
//        jettyContainer.setContextPath("/home");
//        return jettyContainer;
//    }
//
//    @Bean
//    public MongoClientOptions setmongoOptions() {
//        return MongoClientOptions.builder().cursorFinalizerEnabled(false).maxConnectionIdleTime(70000).socketTimeout(70000).build();
//    }


//    @Bean
//    public MongoDatabase timeliDatabase() throws UnknownHostException {
////        MongoProperties properties = new MongoProperties();
////        properties.setUri("mongodb://timelicosmosdb:Mj8JiJqbwZALW4dUkT9VdLY8PUdWhEgUwuog8uuksrZTTHVTBaDzjwKphQMy59y78nf7gvW20b8guKMrtpwuRw==@timelicosmosdb.documents.azure.com:10255/?ssl=true&replicaSet=globaldb, " +
////                "timelicosmosdb:99uurWvtfyBEd0tWIVGUVOYyp9Fdco8By6LFjmh8UPVgo5dVSziiaGSk0NIYldTU12NzXxSYniQ7EuW1GzE7RA==@timelicosmosdb.documents.azure.com:10255/?ssl=true&replicaSet=globaldb");
////        //MongoClient mongoClient = properties.createMongoClient(null, null);
//       // MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://timelicosmosdb:Mj8JiJqbwZALW4dUkT9VdLY8PUdWhEgUwuog8uuksrZTTHVTBaDzjwKphQMy59y78nf7gvW20b8guKMrtpwuRw==@timelicosmosdb.documents.azure.com:10255/?ssl=true&replicaSet=globaldb, " +
//         //       "timelicosmosdb:99uurWvtfyBEd0tWIVGUVOYyp9Fdco8By6LFjmh8UPVgo5dVSziiaGSk0NIYldTU12NzXxSYniQ7EuW1GzE7RA==@timelicosmosdb.documents.azure.com:10255/?ssl=true&replicaSet=globaldb"));
//        MongoClient mongoClient = new MongoClient("localhost", 27017);
//        List<ServerAddress> allAddresses = mongoClient.getAllAddress();
//        System.out.println("Address : "  + allAddresses);
//        //MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://timelicosmosdb:Mj8JiJqbwZALW4dUkT9VdLY8PUdWhEgUwuog8uuksrZTTHVTBaDzjwKphQMy59y78nf7gvW20b8guKMrtpwuRw==@timelicosmosdb.documents.azure.com:10255/?ssl=true&replicaSet=globaldb\n"));
//        return mongoClient.getDatabase("timelidb");
//    }
//
//    @Bean
//    public MongoCollection<Document> personCollection() throws UnknownHostException {
//
//        MongoDatabase timelidb = timeliDatabase();
//        return timelidb.getCollection("person");
//    }

//    public @Bean MongoClient mongoClient() {
//        return new MongoClient(new MongoClientURI("mongodb://timelicosmosdb:Mj8JiJqbwZALW4dUkT9VdLY8PUdWhEgUwuog8uuksrZTTHVTBaDzjwKphQMy59y78nf7gvW20b8guKMrtpwuRw==@timelicosmosdb.documents.azure.com:10255/?ssl=true\n"));
////
//    }
////    public @Bean MongoClient mongoClient() {
////       return new MongoClient(new MongoClientURI("mongodb://timelicosmosdb:Mj8JiJqbwZALW4dUkT9VdLY8PUdWhEgUwuog8uuksrZTTHVTBaDzjwKphQMy59y78nf7gvW20b8guKMrtpwuRw==@timelicosmosdb.documents.azure.com:10255, " +
////                "timelicosmosdb:99uurWvtfyBEd0tWIVGUVOYyp9Fdco8By6LFjmh8UPVgo5dVSziiaGSk0NIYldTU12NzXxSYniQ7EuW1GzE7RA==@timelicosmosdb.documents.azure.com:10255/?ssl=true&replicaSet=globaldb"));
//
////    }
//
//    public @Bean MongoTemplate mongoTemplate() {
//        MongoTemplate timelidb = new MongoTemplate(mongoClient(), "timelidb");
//        timelidb.getDb().addOption(QUERYOPTION_NOTIMEOUT);
//        return timelidb;
//    }
}