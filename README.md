# timeli-middleware

Middleware part of TIMELI code. This project takes queries request from frontend and returns the response by retrieving data from Cosmosdb. Acts as a middleware of frontend and database.

Code is written in Java and Spring Boot framework is used.

1. Run mvn clean install
2. java -jar target/<filename>.jar


